﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public LayerMask enemyMask;
    public float speed = 1;
    Rigidbody2D myBody;
    Transform myTrans;
    float myWidth;

    public GameObject enemySFX;

    public Transform sightStart;
    public Transform sightEnd;
    
    public bool isBlocked;

	void Start ()
    {
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        myWidth = this.GetComponent<SpriteRenderer>().bounds.extents.x;
	}
	
	
	void FixedUpdate ()
    {
        //groundcheck to patrol
        Vector2 lineCastPos = myTrans.position - myTrans.right * myWidth;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);
        
        //bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - myTrans.right, enemyMask); 
        isBlocked = Physics2D.Linecast(sightStart.position, sightEnd.position, enemyMask);

        /*if (isBlocked)
        {
            transform.localScale = new Vector2(transform.localScale.x * - 1, transform.localScale.y);
            speed *= -1;
        }*/

        //Turn around if there's no ground
        if (!isGrounded || isBlocked)
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }
        //Going forward
        Vector2 myVel = myBody.velocity;
        myVel.x = -myTrans.right.x * speed;
        myBody.velocity = myVel;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //meh we got reset boxes ready
            GetComponent<AudioSource>().Play();

            var sfx = Instantiate(enemySFX);
            Destroy(sfx, sfx.GetComponent<AudioSource>().clip.length);
        }
    }
}
