﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Completed : MonoBehaviour {

    public Button next;
    public Button menu;
    public Button restart;

    public EventSystem eventSystem;
    public GameObject selectedObject;

    public CameraFade camera;

    public bool fading;

    // Use this for initialization
    void Start () {
        next = transform.GetChild(2).GetComponent<Button>();
        menu = transform.GetChild(3).GetComponent<Button>();
        restart = transform.GetChild(4).GetComponent<Button>();

        next.onClick.AddListener(Next);
        menu.onClick.AddListener(Menu);
        restart.onClick.AddListener(Restart);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(selectedObject);

        camera = GameObject.Find("Camera").GetComponent<CameraFade>();

        //check if existing score is higher, if it is then dont save the new score
        if(!PlayerPrefs.HasKey(SceneManager.GetActiveScene().buildIndex.ToString() + "max")

            || PlayerPrefs.GetFloat(SceneManager.GetActiveScene().buildIndex.ToString() + "max")
            < GameObject.Find("Player").GetComponent<StatManager>().score)
        {

            //save the score as a string for print and as a float for checking highest score
            PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex.ToString(),
            (GameObject.Find("Player").GetComponent<StatManager>().score
            + "/" + (GameObject.FindGameObjectsWithTag("Coin").Length
            + GameObject.Find("Player").GetComponent<StatManager>().score)));

            PlayerPrefs.SetFloat(SceneManager.GetActiveScene().buildIndex.ToString() + "max", GameObject.Find("Player").GetComponent<StatManager>().score);
        }

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        

        if (Input.GetAxisRaw("Vertical") != 0 && (eventSystem.currentSelectedGameObject == null || !eventSystem.currentSelectedGameObject.activeSelf))
        {
            eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject = selectedObject);
        }
    }

    void Next()
    {

        camera.FadeOut();
        StartCoroutine(FadeOut(SceneManager.GetActiveScene().buildIndex + 1));
        
    }

    void Menu()
    {
        camera.FadeOut();
        StartCoroutine(FadeOut(0));
    }

    void Restart()
    {
        var player = GameObject.Find("Player").GetComponent<PlayerController>();
        player.canMove = true;
        Destroy(this.gameObject);
    }

    public IEnumerator FadeOut(int index)
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(index);
    }
    
}
