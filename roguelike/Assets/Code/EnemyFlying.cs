﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlying : MonoBehaviour
{

    public LayerMask enemyMask;
    public float speed = 1;
    Rigidbody2D myBody;
    Transform myTrans;
    float myWidth;

    public Transform sightStart;
    public Transform sightEnd;

    public bool isBlocked;

    private Transform currentPoint;
    public Transform[] points;
    public int pointSelection;

    public GameObject FlyingSFX;

    void Start()
    {
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        myWidth = this.GetComponent<SpriteRenderer>().bounds.extents.x;
        currentPoint = points[pointSelection];
    }


    void FixedUpdate()
    {
        /*//groundcheck to patrol
        Vector2 lineCastPos = myTrans.position - myTrans.right * myWidth;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);

        //bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - myTrans.right, enemyMask); */
        //isBlocked = Physics2D.Linecast(sightStart.position, sightEnd.position, enemyMask);

        /*if (isBlocked)
        {
            transform.localScale = new Vector2(transform.localScale.x * - 1, transform.localScale.y);
            speed *= -1;
        }*/

        //moving
        myBody.transform.position = Vector3.MoveTowards(myBody.transform.position, currentPoint.position, Time.deltaTime * speed);

        if (myBody.transform.position == currentPoint.position)
        {
            pointSelection++;
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);

            //check if there's no more points in array and reset it to 0
            if (pointSelection == points.Length)
            {
                pointSelection = 0;

            }

            currentPoint = points[pointSelection];
        }
        /*//Turn around if there's no ground
        if (isBlocked)
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }
        //Going forward
        Vector2 myVel = myBody.velocity;
        myVel.x = -myTrans.right.x * speed;
        myBody.velocity = myVel;*/
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            GetComponent<AudioSource>().Play();

            var sfx = Instantiate(FlyingSFX);
            Destroy(sfx, sfx.GetComponent<AudioSource>().clip.length);
        }
    }
}
