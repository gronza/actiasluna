﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustAnimation : MonoBehaviour {

    public GameObject dustEffect;

	// Use this for initialization
	void Start () {
        dustEffect = GameObject.Find("DustAnimationObject");
        
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Dust()
    {
        var flipped = 1;
        if (gameObject.GetComponent<SpriteRenderer>().flipX == true)
        {
            flipped = -1;
        }

        var position = new Vector3(
            transform.position.x - flipped * gameObject.GetComponent<BoxCollider2D>().size.x/2,
            transform.position.y - GetComponent<SpriteRenderer>().bounds.size.y/2 - 0.1f);
    

        var dust = 
            Instantiate(dustEffect,
            position,
            new Quaternion());

        if(flipped == 1)
        {
            dust.GetComponent<SpriteRenderer>().flipX = true;
        }

        Destroy(dust, dustEffect.GetComponent<Animation>().clip.length);
    }
}
