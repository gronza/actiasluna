﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject {

    bool jumping = false;
    bool falling = false;
    float jumpTimer = 0f;

    public bool canMove = true;
    public bool deathAnim;
    public float speed = 5;
    public float slideTimer;
    public bool slowing;
    public float slowSpeed;
    public float runTimer;
    public float runTimerMin = 0.3f;

    public bool jumpBuffer;
    public float bufferTimer;
    public float bufferTime = 0.2f;
    public float jumpBufferTimer;
    public bool shortHop;

    public bool ignoreFloor;

    public bool onPlatform = false;

    public bool checkGrounded = false;

    public Animator animator;

    //particle effects
    public DustAnimation duster;
    public JumpAnimation jumper;
    public FallAnimation fallen;
    

    public Transform checkPoint;

    public AudioClip jump1;
    public AudioClip jump2;
    public AudioClip jump3;
    public AudioClip jump4;
    public AudioClip[] jumps;

    public AudioClip death;

    public AudioClip footstep1;
    public AudioClip footstep2;
    public AudioClip[] footsteps;

    public AudioClip landing1;
    public AudioClip landing2;
    public AudioClip[] landings;

    public AudioClip win;

    AudioSource source;

    public CameraFade camera;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        gravityModifier = 2;
        duster = gameObject.GetComponent<DustAnimation>();
        jumper = gameObject.GetComponent<JumpAnimation>();
        fallen = gameObject.GetComponent<FallAnimation>();
        checkPoint = transform;
        source = GetComponent<AudioSource>();

        //put the soundclips to the corresponding arrays
        jumps = new AudioClip[4];
        jumps[0] = jump1;
        jumps[1] = jump2;
        jumps[2] = jump3;
        jumps[3] = jump4;
        
        footsteps = new AudioClip[2];
        footsteps[0] = footstep1;
        footsteps[1] = footstep2;

        landings = new AudioClip[2];
        landings[0] = landing1;
        landings[1] = landing2;

        camera = GameObject.Find("Camera").GetComponent<CameraFade>();
    }

    // Update is called once per frame
     void Update () {

        //base.FixedUpdate();
        //making 1 way platforms with a cheap way
        /*
        if (velocity.y > 0)
        {
            var platforms = GameObject.FindGameObjectsWithTag("Platform");

            for (int i = 0; i < platforms.Length; i++)
            {
                platforms[i].GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        else
        {
            var platforms = GameObject.FindGameObjectsWithTag("Platform");

            for (int i = 0; i < platforms.Length; i++)
            {
                platforms[i].GetComponent<BoxCollider2D>().enabled = true;
            }
        }
        */

        if (source.clip == footstep1 || source.clip == footstep2)
        {
            source.volume = 0.5f;
        }
        else
        {
            source.volume = 1;
        }

        animator.SetBool("Jump", false);

        if (canMove)
        {
            Move();
            Slow();
            Jump();
        }
        else
        {
            
            
            if (deathAnim)
            {
                targetVelocity.x = 0;
                velocity.x = 0;
                targetVelocity.y = 0;
                velocity.y = 0;
                transform.Rotate(new Vector3(0, 0, 10));
                transform.Translate(new Vector3(0, 0.05f), Space.World);
                
            }
        }

        SetAnimatorParameters();
        
        if(runTimer > 1 && grounded)
        {
            runTimer = runTimerMin;
            duster.GetComponent<DustAnimation>().Dust();
        }
 
        if (Input.GetButtonDown("Jump") && grounded)
        {
            //jumper.GetComponent<JumpAnimation>().Jump();
            falling = true;

        }

        if(grounded != checkGrounded)
        {
            checkGrounded = grounded;

            if (checkGrounded)
            {
                source.clip = landings[Random.Range(0, 2)];
                source.Play();
            }
        }
        
    }
    

    void Move()
    {

        //set animation to running is player is moving
        animator.SetBool("isMoving", false);

        if(Input.GetAxis("Horizontal") < 0)
        {
            targetVelocity = Vector2.left * speed;
            GetComponent<SpriteRenderer>().flipX = true;

            animator.SetBool("isMoving", true);

            //check how long the player has been running
            runTimer += Time.deltaTime;
            slowing = false;
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            targetVelocity = Vector2.right * speed;
            GetComponent<SpriteRenderer>().flipX = false;

            animator.SetBool("isMoving", true);

            //check how long the player has been running
            runTimer += Time.deltaTime;
            slowing = false;
        }

        if (Input.GetAxis("Horizontal") != 0 && grounded)
        {
            if(source.clip != footstep1 && source.clip != footstep2)
            {
                source.Stop();
            }
            if (!source.isPlaying)
            {
                if (source.clip != footsteps[1])
                {
                    source.clip = footsteps[1];
                }
                else
                {
                    source.clip = footsteps[0];
                }

                
                source.Play();

            }
        }
        //slide with arrows tooo
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)
            || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {

            if (runTimer > runTimerMin)
            {
                slowSpeed = 1.2f;
            }
            else { slowSpeed = 3; }

            runTimer = 0;

            slowing = true;

            source.Stop();
        }
    }

    //slow x movement periodically instead of stopping instantly
    void Slow()
    {
        if (slowing)
        {
            targetVelocity.x = targetVelocity.x / slowSpeed;

            if(Mathf.Abs(targetVelocity.x) < 0.1f)
            {
                targetVelocity.x = 0;
                slowing = false;
            }
        }
    }

    void Jump()
    {
        bufferTimer -= Time.deltaTime;
        if(bufferTimer <= 0)
        {
            jumpBuffer = false;
        }
        
        //buffer a jump if command is given in the air
        if(Input.GetButtonDown("Jump") && !grounded){
            jumpBuffer = true;
            bufferTimer = bufferTime;
        }
        if (Input.GetButton("Jump") && jumpBuffer)
        {
            jumpBufferTimer += Time.deltaTime;
        }
        if (Input.GetButtonUp("Jump") && jumpBuffer)
        {
            if(jumpBufferTimer < 0.1f)
            {
                shortHop = true;
            }
            jumpBufferTimer = 0;
        }


            if ((Input.GetButtonDown("Jump") || jumpBuffer) && grounded)
        {
            animator.SetBool("Jump", false);
            velocity.y = 10;
            falling = true;

            source.clip = jumps[Random.Range(0, 4)];
            source.Play();
        }
        else if ((Input.GetButtonUp("Jump") && velocity.y > 8f)
            || jumpBuffer && shortHop)
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * .5f;
                shortHop = false;
            }

            
        }

        
    }

    public void ResetToCheckpoint()
    {
        source.clip = death;
        
        source.Play();
        canMove = false;
        camera.FadeOut(1.5f);
        StartCoroutine(FadeOut());

        GameObject.Find("CM vcam2").GetComponent<Cinemachine.CinemachineVirtualCamera>().Follow = null;
    }

    public IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(3);

        deathAnim = false;
        this.GetComponent<BoxCollider2D>().enabled = true;
        canMove = true;
        targetVelocity.x = 0;
        velocity.x = 0;
        transform.rotation= new Quaternion(0, 0, 0, 0);

        camera.FadeIn(1.5f);
        transform.position = checkPoint.position;

        //boss fight stuffs here
        if (GameObject.Find("Boss") != null)
        {
            Debug.Log("boss deth");
            GameObject.Find("Boss").GetComponent<Boss>().Reset();
        }

        //create new virtual camera at checkpoint
        var newC = Instantiate<GameObject>(GameObject.Find("CM vcam2"),
            checkPoint.transform.position, new Quaternion());

        Destroy(GameObject.Find("CM vcam2"));

        newC.name = "CM vcam2";
        newC.GetComponent<Cinemachine.CinemachineVirtualCamera>().Follow = this.transform;

        
    }

    /*
    void AvoidOverlap()
    {
        if (GetComponent<BoxCollider2D>().IsTouching(
                    touchCollider))
        {
            Debug.Log("Player in Collider!");
            transform.Translate
                (new Vector3(0, -0.3f), Space.World);
        }
    }
    */

    public void SetAnimatorParameters()
    {
        if (grounded)
        {
            animator.SetBool("isGrounded", true);
        }
        else
        {
            animator.SetBool("isGrounded", false);
        }
        animator.SetFloat("Velocity", velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground")
            || collision.gameObject.CompareTag("Platform")
            || collision.gameObject.CompareTag("MovingPlatform"))
        {
            runTimer = runTimerMin;
            if (falling && grounded)
            {
                //fallen.GetComponent<FallAnimation>().Fall();
                falling = false;

            }
            
        }
        
        //Checking collision with MovingPlatform tag and make it parent to stay on and with it :3
        //aika ovelaa :D kjeh kjeh
        //I know right ;D
        
    }
    

    private void OnCollisionStay2D(Collision2D collision)
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Reset"))
        {

            transform.parent = null;
            this.GetComponent<BoxCollider2D>().enabled = false;
            if (collision.gameObject.layer == 9)
            {
                deathAnim = true;
            }
            ResetToCheckpoint();

            
        }

        if (collision.gameObject.CompareTag("BossReset"))
        {
            GameObject.Find("Boss").GetComponent<Boss>().Reset();

            transform.parent = null;
            this.GetComponent<BoxCollider2D>().enabled = false;
            if (collision.gameObject.layer == 9)
            {
                deathAnim = true;
            }
            ResetToCheckpoint();
        }

        //fix the moving platform bug with trigger
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Extrapolate;
            transform.parent = collision.transform.parent.transform;
        }

        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Platform")
            || collision.gameObject.CompareTag("MovingPlatform"))
        {
        }

        //fix the moving platform bug with trigger
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
            transform.parent = null;
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        
    }

    public override void HitGround() {
        
    }
}
