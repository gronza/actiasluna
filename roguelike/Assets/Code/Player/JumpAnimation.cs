﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAnimation : MonoBehaviour {

    public GameObject jumpEffect;

	// Use this for initialization
	void Start () {
        jumpEffect = GameObject.Find("jumpAnimation"); 
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Jump()
    {
        var flipped = 1;
        if (gameObject.GetComponent<SpriteRenderer>().flipX == true)
        {
            flipped = -1;
        }

        var position = new Vector3(
            transform.position.x - flipped * gameObject.GetComponent<BoxCollider2D>().size.x / 2,
            transform.position.y - GetComponent<SpriteRenderer>().bounds.size.y/2 - 0.1f);
    
        var jump = 
        Instantiate(
            jumpEffect,
            position,
            new Quaternion()
            );

        if (flipped == 1)
        {
            jump.GetComponent<SpriteRenderer>().flipX = true;
        }
        Debug.Log(jumpEffect.GetComponent<Animation>().clip.length);
        Destroy(jump, jumpEffect.GetComponent<Animation>().clip.length);
    }
}