﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StatManager : MonoBehaviour {

    public float score;
    public Text scoreText;

	// Use this for initialization
	void Start () {

        if (SceneManager.GetActiveScene().buildIndex != 8)
        {
            scoreText = GameObject.Find("Score").GetComponent<Text>();
        }

        if (!PlayerPrefs.HasKey(SceneManager.GetActiveScene().buildIndex.ToString()))
        {
            PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex.ToString(),
                "0/" + GameObject.FindGameObjectsWithTag("Coin").Length);
        }

        if (!PlayerPrefs.HasKey(SceneManager.GetActiveScene().buildIndex.ToString() + "bugs"))
        {
            PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex.ToString() + "bugs",
                "0/" + GameObject.Find("LevelEnding").GetComponent<LevelManager>().bugsMax);
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (SceneManager.GetActiveScene().buildIndex != 8)
        {
            scoreText.text = score.ToString() + "/"
            + (GameObject.FindGameObjectsWithTag("Coin").Length
            + GameObject.Find("Player").GetComponent<StatManager>().score);
        }
	}
}
