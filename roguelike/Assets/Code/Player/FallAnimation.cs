﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallAnimation : MonoBehaviour {

    public GameObject fallEffect;

	// Use this for initialization
	void Start () {
        fallEffect = GameObject.Find("DustAnimationObject"); 
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Fall()
    {

        var position1 = new Vector3(
            transform.position.x + (0.6f * gameObject.GetComponent<BoxCollider2D>().size.x / 2),
            transform.position.y - GetComponent<SpriteRenderer>().bounds.size.y / 2 - 0.1f);

        var position2 = new Vector3(
            transform.position.x - (0.6f * gameObject.GetComponent<BoxCollider2D>().size.x / 2),
            transform.position.y - GetComponent<SpriteRenderer>().bounds.size.y / 2 - 0.1f);

        var fall1 =
            Instantiate(fallEffect,
            position1,
            new Quaternion());

        var fall2 = 
            Instantiate(fallEffect,
            position2,
            new Quaternion());

        fall2.GetComponent<SpriteRenderer>().flipX = true;

        Destroy(fall1, fallEffect.GetComponent<Animation>().clip.length);
        Destroy(fall2, fallEffect.GetComponent<Animation>().clip.length);
    }
}