﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ps_Seek : MonoBehaviour 
{
    public Transform target;
    public float force = 10.0f;
    public float killDistance = 5f;

    ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();
	}
	
	// We want the whole particle effect to be complete before applyin seeking
	void LateUpdate() {

        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps.particleCount];

        ps.GetParticles(particles);

        for (int u = 0; u < particles.Length; u++)
        {
            ParticleSystem.Particle p = particles[u];

            Vector3 particleWorldPosition;

            // Accounting for potential particle effect settings in Unity
            if (ps.main.simulationSpace == ParticleSystemSimulationSpace.Local)
            {
                particleWorldPosition = transform.TransformPoint(p.position);

            }
            else if (ps.main.simulationSpace == ParticleSystemSimulationSpace.Custom)
            {
                particleWorldPosition = ps.main.customSimulationSpace.TransformPoint(p.position);

            } else
            {
                particleWorldPosition = p.position;
            }

            Vector3 directionToTarget = (target.position - particleWorldPosition).normalized;

            Vector3 seekForce = (directionToTarget * force) * Time.deltaTime;

            p.velocity += seekForce;

            particles[u] = p;

            if (Vector3.Distance(p.position+ps.transform.position, target.position) < killDistance){
                particles[u].remainingLifetime = 0f;
            }
        }

        ps.SetParticles(particles, particles.Length);
	}
}
