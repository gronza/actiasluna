﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerController : MonoBehaviour {

    public GameObject platform;
    public GameObject seed;
    public GameObject flower;
    public Animator flowerAnim;
    public GameObject varsi;
    public GameObject varsiSpawn;
    public bool spawn;
    public GameObject lowestVarsi;

    public bool growing;
    public float growTime = 2;
    public float growTimer = 0;
    public float timer;

    public bool platformActive = false;
    public bool decaying = false;

	// Use this for initialization
	void Start () {
        platform = transform.GetChild(0).gameObject;
        seed = transform.GetChild(1).gameObject;

        platform.transform.localPosition = new Vector3(0, 0);
        platform.transform.localScale = new Vector3(0, 0);

        platform.gameObject.GetComponent<BoxCollider2D>().enabled = false;

        flowerAnim = flower.GetComponent<Animator>();
        
    }
	
	// Update is called once per frame
	void Update () {
        Grow();
        timer -= Time.deltaTime;
        if(timer <= 0 && platformActive)
        {
            platformActive = false;
            decaying = true;
        }
        Decay();
	}

    public void Grow()
    {
        if (growing)
        {
            //stop growing at right time
            if (growTimer >= growTime)
            {
                platform.transform.localScale = new Vector3(1, 1);
                platform.transform.localPosition = new Vector3(0, 3);
                platform.gameObject.GetComponent<BoxCollider2D>().enabled = true;



                timer = 5;
                growing = false;
                platformActive = true;

                GetComponent<AudioSource>().Stop();
                flowerAnim.SetBool("Growing", false);
                return;
            }


            if (!GetComponent<AudioSource>().isPlaying)
            GetComponent<AudioSource>().Play();

            flowerAnim.SetBool("Growing", true);
            flowerAnim.SetBool("Decaying", false);


            platform.transform.localScale += new Vector3(1 * Time.deltaTime / growTime, 1 * Time.deltaTime / growTime);
            platform.transform.position += new Vector3(0, 3 * Time.deltaTime  / growTime);
            flower.transform.position += new Vector3(0, 3 * Time.deltaTime / growTime);

            growTimer += Time.deltaTime;
            


            if (lowestVarsi != null)
            {
                if (lowestVarsi.transform.position.y - varsi.GetComponent<SpriteRenderer>().bounds.size.y + 0.03f
                    > varsiSpawn.transform.position.y)
                {
                    spawn = true;
                }
            }
            
            //create a varsi under the plant
            if (spawn)
            {
                var low = Instantiate<GameObject>(varsi, varsiSpawn.transform.position,
                    new Quaternion(), lowestVarsi.transform);
                lowestVarsi = low;
                spawn = false;
            }

            
        }
    }

    public void Decay()
    {
        if (decaying)
        {
            platform.transform.localScale -= new Vector3(1 * Time.deltaTime / growTime, 1 * Time.deltaTime / growTime);
            platform.transform.position -= new Vector3(0, 3 * Time.deltaTime / growTime);
            flower.transform.position -= new Vector3(0, 3 * Time.deltaTime / growTime);
            growTimer -= Time.deltaTime;
            platform.gameObject.GetComponent<BoxCollider2D>().enabled = false;

            flowerAnim.SetBool("Decaying", true);
            
            //destroy varsi's
            if(lowestVarsi.transform.position.y
                < varsiSpawn.transform.position.y)
            {
                if (lowestVarsi.CompareTag("Varsi"))
                {
                    var ded = lowestVarsi;
                    lowestVarsi = ded.transform.parent.gameObject;

                
                    Destroy(ded);
                }
                else
                {
                    lowestVarsi = flower;
                }
            }


            if (growTimer <= 0)
            {
                platform.transform.localScale = new Vector3(0, 0);
                platform.transform.localPosition = new Vector3(0, 0);
                
                
                decaying = false;
                platformActive = false;
                spawn = true;

                flowerAnim.SetBool("Decaying", false);
            }
        }
    }
    
}
