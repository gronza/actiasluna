﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour {

    public AudioClip menu;
    public AudioClip intro;
    public AudioClip levels1;
    public AudioClip levels2;
    public AudioClip levels3;

    public AudioSource source;

    public float volume = 1;
    public bool fade = false;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        var index = (SceneManager.GetActiveScene().buildIndex);
        source.volume = volume;

        if (index == 0)
        {
            if(source.clip != menu)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = menu;
                    source.Play();
                    fade = false;
                }
            }
            
        }

        if (index > 0 && index < 3)
        {
            if (source.clip != intro)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = intro;
                    source.Play();
                    fade = false;
                }
            }

        }

        if (index > 2 && index < 5)
        {
            if (source.clip != levels1)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = levels1;
                    source.Play();
                    fade = false;
                }
            }

        }

        if (index == 5)
        {
            if (source.clip != levels2)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = levels2;
                    source.Play();
                    fade = false;
                }
            }

        }

        if (index > 5 && index < 9)
        {
            if (source.clip != levels3)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = levels3;
                    source.Play();
                    fade = false;
                }
            }

        }

        if (index == 9)
        {
            if (source.clip != intro)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = intro;
                    source.Play();
                    fade = false;
                }
            }

        }

        if(!fade && volume < 0.5f)
        {
            volume += Time.deltaTime;
        }

        //SceneManager.activeSceneChanged;
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Fade()
    {

    }
}
