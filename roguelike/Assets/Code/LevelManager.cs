﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public Vector3 spawnPosition;
    public Transform playerTransform;

    public GameObject endScreen;


    public float bugs;
    public float bugsMax;

    //Reset position if falling too low, could be done better
    private void Update()
    {
        
    }
    
    //Scenemanager, load next scene
    //public int index; //load from index number
    [SerializeField] public string loadlevel;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponent<AudioSource>().Play();

            //SAVE PLAYER PROGRESS
            if (PlayerPrefs.GetInt("levels") < SceneManager.GetActiveScene().buildIndex)
            {
                PlayerPrefs.SetInt("levels", SceneManager.GetActiveScene().buildIndex);
            }


            Instantiate<GameObject>(endScreen,
                GameObject.Find("Canvas").transform.position,
                new Quaternion(),
                GameObject.Find("Canvas").transform);

            var player = GameObject.Find("Player").GetComponent<PlayerController>();
            player.canMove = false;
            player.targetVelocity.x = 0;
            player.velocity.x = 0;
            player.targetVelocity.y = 0;
            player.velocity.y = 0;
            player.animator.SetBool("isMoving", false);

            if (!PlayerPrefs.HasKey(SceneManager.GetActiveScene().buildIndex.ToString() + "maxBugs")

            || PlayerPrefs.GetFloat(SceneManager.GetActiveScene().buildIndex.ToString() + "maxBugs")
            < bugs)
            {

                //save the score as a string for print and as a float for checking highest score
                PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex.ToString() + "bugs",
                bugs + "/" + bugsMax);

                PlayerPrefs.SetFloat(SceneManager.GetActiveScene().buildIndex.ToString() + "maxBugs", bugs);
            }
        }
    }
}