﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendlyBug : MonoBehaviour {


    public GameObject soundEffect;
    //This one for pause menu image
    //public GameObject bugImage;

    //UI image & particle
    public GameObject bugUiImage;
    public GameObject particle;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            GetComponent<AudioSource>().Play();

            var se = Instantiate(soundEffect);
            Destroy(se, se.GetComponent<AudioSource>().clip.length);

            //pause menu collectibles
            //bugImage.GetComponent<Image>().color = Color.white;

            //uiimage
            bugUiImage.GetComponent<Image>().color = Color.white;
            //let the there be particles
            particle.GetComponent<ParticleSystem>().Play();
            Debug.Log("Bug +1 yay");
            Destroy(gameObject);

            GameObject.Find("LevelEnding").GetComponent<LevelManager>().bugs++;
        }
    }
}
