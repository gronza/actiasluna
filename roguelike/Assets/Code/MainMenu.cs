﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {


    public GameObject mainMenuUI;
    //public Canvas creditBox;
    public GameObject levelSelect;

    public EventSystem eventSystem;
    public GameObject selectedObject;
    public GameObject mainFirst;
    public GameObject creditsFirst;
    public GameObject exitBoxFirst;
    public GameObject levelSelectFirst;
    private bool buttonSelected;

    private void Start()
    {
        //creditBox = creditBox.GetComponent<Canvas>();
        //creditBox.enabled = false;
        eventSystem.firstSelectedGameObject = mainFirst;

    }
    private void Awake()
    {
        eventSystem.SetSelectedGameObject(null);
        eventSystem.firstSelectedGameObject = mainFirst;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(mainFirst);
    }

    void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && (eventSystem.currentSelectedGameObject == null || !eventSystem.currentSelectedGameObject.activeSelf))
        {
            eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);
        }
        /*if (Input.GetAxisRaw ("vertical") != 0 && buttonSelected == false)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            buttonSelected = true;
        }*/
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }

    //Start Game from level 1 
    public void StartLevel()
    {
        levelSelect.SetActive(true);
        gameObject.SetActive(false);
        eventSystem.SetSelectedGameObject(null);
        eventSystem.firstSelectedGameObject = levelSelectFirst;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(levelSelectFirst);
    }

    //credits
    public void Credits()
    {
        Debug.Log("Who did what now");
        eventSystem.SetSelectedGameObject(null);
        eventSystem.firstSelectedGameObject = creditsFirst;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsFirst);

    }
    
    //Exit Game
    public void ExitGame()
    {
        Debug.Log("Shutdown");
        Application.Quit();

    }
    public void Return()
    {
        //creditBox.enabled = false;
        eventSystem.SetSelectedGameObject(null);
        eventSystem.firstSelectedGameObject = mainFirst;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(mainFirst);
    }
    public void ExitConf()
    {
        eventSystem.SetSelectedGameObject(null);
        eventSystem.firstSelectedGameObject = exitBoxFirst;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(exitBoxFirst);
    }
}
