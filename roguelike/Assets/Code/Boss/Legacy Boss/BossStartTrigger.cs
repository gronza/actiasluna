﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Boss").GetComponent<BossController>().pos1 = true;
            GameObject.Find("Boss").GetComponent<BossController>().canFire = true;
            GameObject.Find("testHyvaaPuuda (1)").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("testHyvaaPuuda (1)").GetComponent<BoxCollider2D>().enabled = true;
            Destroy(gameObject);
        }
    }
}
