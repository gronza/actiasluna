﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BossController : MonoBehaviour {

    public float floatTimer;
    public bool floatGrowing;
    public bool movingUp;
    private float speed = 1f;

    public bool pos1 = false;

    public int health = 3;

    public GameObject leftSide;
    public GameObject rightSide;
    public GameObject ball;
    public bool canFire = false;
    public float fireTimer;
    public int damageDone;

	// Use this for initialization
	void Start () {
        floatTimer = 0;
        floatGrowing = true;

        leftSide = GameObject.Find("LeftSide");
        rightSide = GameObject.Find("RightSide");

        canFire = false;
        health = 3;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Float();
        MoveToPosition();
        if(health <= 0)
        {
            Die();
        }

        fireTimer += Time.deltaTime;
        if(fireTimer > 3)
        {
            FireBall();
            fireTimer = 0;
        }

        if(damageDone >= 3)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        GameObject.Find("Score").GetComponent<Text>().text = "Health: " + (3 - damageDone).ToString();
    }

    private void MoveToPosition()
    {
        if (pos1)
        {
            if (transform.position.x >= GameObject.Find("Position1").transform.position.x)
            {
                transform.Translate(new Vector3(-speed * 5 * Time.deltaTime, 0));
            }

            if (leftSide.active)
            {
                leftSide.active = false;
            }

            if (!rightSide.active)
            {
                rightSide.active = true;
            }
        }
        else if (!pos1)
        {
            if (transform.position.x <= GameObject.Find("Position2").transform.position.x)
            {
                transform.Translate(new Vector3(speed * 5 * Time.deltaTime, 0));
            }

            if (rightSide.active)
            {
                rightSide.active = false;
            }

            if (!leftSide.active)
            {
                leftSide.active = true;
            }
        }
    }

    private void Float()
    {
        if (floatGrowing)
        {
            floatTimer += Time.deltaTime;
        }
        else
        {
            floatTimer -= Time.deltaTime;
        }

        if (floatTimer >= 1)
        {
            floatGrowing = false;
        }
        else if (floatTimer <= 0.1f)
        {
            floatGrowing = true;
            movingUp = !movingUp;
        }

        if (movingUp)
        {
            transform.Translate(new Vector3(0, floatTimer * speed * Time.deltaTime), Space.Self);
        }
        else if (!movingUp)
        {
            transform.Translate(new Vector3(0, -floatTimer * speed * Time.deltaTime), Space.Self);
        }
    }

    void Die()
    {
        leftSide.active = true;
        rightSide.active = true;
        Destroy(GameObject.Find("testHyvaaPuuda"));
        Destroy(gameObject);
    }

    void FireBall()
    {
        if (canFire)
        {
            var fireBall = Instantiate(ball,
                transform.position,
                new Quaternion()
                );
            
            fireBall.GetComponent<FireBall>().dir = (GameObject.Find("Player").transform.position - transform.position).normalized;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            pos1 = !pos1;
            health--;
        }
    }
}
