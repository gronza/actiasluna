﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

    public float speed;
    public float rotateSpeed;

    public float resetOffset;
    public float resetFreezeTime;
    public float moveTimer;

    public bool deathAnim;

    public PlayerController player;

    public GameObject vcam;

    public bool waiting = true;
    public bool descending;
    public bool ascending;


	// Use this for initialization
	void Start () {

        speed = 3f;
        rotateSpeed = 30f;
        resetOffset = 12;
        resetFreezeTime = 1.5f;

        player = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (deathAnim)
        {
            GameObject.Find("Camera").transform.parent = null;
            vcam.SetActive(true);
            GetComponent<BoxCollider2D>().enabled = false;
            transform.Rotate(new Vector3(0, 0, 10));
            transform.Translate(new Vector3(-0.025f, 0.05f), Space.World);
            transform.localScale = transform.localScale*0.8f;
            
        }
        else
        {
            Move();
        }
    }

    public void Reset()
    {
        transform.position = new Vector3(player.checkPoint.position.x - resetOffset, player.transform.position.y);
        moveTimer = resetFreezeTime;
    }

    public void Move()
    {
        
        moveTimer -= Time.deltaTime;
        if (moveTimer < 0)
        {
            if (waiting)
            {
                GetComponentInChildren<Animator>().enabled = false;
                return;
            }

            if (!ascending) {
                var spinning = true;
                if (transform.rotation.z >= 0)
                {
                    spinning = false;
                    transform.rotation = new Quaternion(0, 0, 0, 0);

                }
                if (spinning)
                    transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime), Space.World);
            }

            if (descending)
            {
                GetComponentInChildren<Animator>().enabled = true;

                transform.Translate(new Vector3(speed * Time.deltaTime, 0), Space.Self);

                if (transform.position.y <= player.transform.position.y)
                {
                    descending = false;
                }
            }
            if (ascending)
            {
                transform.Translate(new Vector3(speed * Time.deltaTime, 0), Space.Self);
                var spinning = true;
                if (transform.rotation.z >= 1f)
                {
                    spinning = false;
                    transform.rotation = new Quaternion(0, 0, 1f, 0);

                }
                if (spinning)
                    transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime), Space.World);
            }
            else
            {


                //transform.Translate(new Vector3(speed, 0));
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            }
        }
    }

    public void Die()
    {
        this.GetComponent<BoxCollider2D>().enabled = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Finish"))
        {
            ascending = true;
            this.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
