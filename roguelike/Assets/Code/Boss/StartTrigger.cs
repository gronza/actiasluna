﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTrigger : MonoBehaviour {

    public GameObject boss;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            boss.GetComponent<Boss>().waiting = false;
            boss.GetComponent<Boss>().descending = true;

            Destroy(this);
            //GameObject.Find("CM vcam2").gameObject.SetActive(false);
            //GameObject.Find("Camera").transform.parent = boss.transform;
        }


    }
}
