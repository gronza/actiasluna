﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreFloor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        //1way platforms boys
        if (collision.gameObject.CompareTag("1way"))
        {
            Debug.Log("the way is open");
            

            //transform.parent.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            collision.transform.parent.GetComponent<BoxCollider2D>().enabled = false;
            //collision.transform.parent.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //1way platforms boys
        if (collision.gameObject.CompareTag("1way"))
        {
            Debug.Log("the way is shut");

            collision.transform.parent.GetComponent<BoxCollider2D>().enabled = true;
            //collision.transform.parent.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    
}
