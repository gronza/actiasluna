﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Collected : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var max = GameObject.FindGameObjectsWithTag("Coin").Length
            + GameObject.Find("Player").GetComponent<StatManager>().score;

        GetComponent<Text>().text = "Blue stuff collected:\n"
            + GameObject.Find("Player").GetComponent<StatManager>().score.ToString()
            + "/" + max;
    }
	
	// Update is called once per frame
	void Update () {

        var max = GameObject.FindGameObjectsWithTag("Coin").Length
            + GameObject.Find("Player").GetComponent<StatManager>().score;

        GetComponent<Text>().text = "Blue stuff collected:\n"
            + GameObject.Find("Player").GetComponent<StatManager>().score.ToString()
            + "/" + max;

        if(SceneManager.GetActiveScene().buildIndex == 8)
        {
            GetComponent<Text>().text = "Boss defeated!";
        }
    }
}
