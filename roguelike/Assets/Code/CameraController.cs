﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform player;
    public float staticDistanceX;
    public float staticDistanceY;
    public bool moveCameraX;
    public bool moveCameraY;

    public bool left;
    public bool transfering;
    public float transferSpeed = 5;

    public bool mid;
    public bool transferingY;
    public float transferSpeedY = 5f;
    public float startTime;
    public float journeyLength;
    public Vector3 startPoint;
    public Vector3 targetPoint;
    public float SPEED = 1;
    public float fixFloat = 1f;

    public float startTimeX;
    public float journeyLengthX;
    public Vector3 startPointX;
    public Vector3 targetPointX;


    public float x;
    private float y = 3;

	// Use this for initialization
	public void Start () {
		player = GameObject.Find("Player").transform;
        transform.position = new Vector3(player.position.x, player.position.y, -1);

        left = false;
        transfering = false;

        mid = false;
        transferingY = false;

        staticDistanceX = 3f;
        x = 2;

        staticDistanceY = 2f;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        

    
        //X AXIS CAMERA MOVEMENT
        if (!transfering)
        {
            if(left)
            targetPointX = new Vector3(player.position.x + x, transform.position.y);

            if (left && player.position.x <= transform.position.x + x)
            {
                transform.position = new Vector3(player.position.x - x,
                    transform.position.y, -1);
            }
            if (left && player.position.x >= transform.position.x + staticDistanceX)
            {
                

                left = false;
                transfering = true;
            }
        }
        if (!transfering) {

            if (!left)
                targetPointX = new Vector3(player.position.x - x, transform.position.y, -1);

            if (!left && player.position.x >= transform.position.x - x)
            {
                transform.position = new Vector3(player.position.x + x,
                    transform.position.y, -1);

                
            }
            if (!left && player.position.x <= transform.position.x - staticDistanceX)
            {
                left = true;
                transfering = true;
            }
        }
        if(transfering)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -1);
            


            if (left)
            {
                targetPointX = new Vector3(player.position.x - x, transform.position.y, -1);
            }
            else
            {
                targetPointX = new Vector3(player.position.x + x, transform.position.y, -1);
            }

            transferSpeed = Vector3.Distance(transform.position, targetPointX) * Time.deltaTime * SPEED;
            
            transform.position += Vector3.MoveTowards(transform.position, targetPointX, transferSpeed) - transform.position;

            

                if (transform.position.x <= targetPointX.x + fixFloat && transform.position.x >= targetPointX.x - fixFloat)
                {
                    Debug.Log("Hai again");
                    transfering = false;
                }


        }

        //Y AXIS CAMERA MOVEMENT

        if (!transferingY)
        {
            targetPoint = new Vector3(transform.position.x, player.position.y, -1);

            if (player.GetComponent<PlayerController>().onPlatform && player.GetComponent<PlayerController>().grounded)
                {
                    mid = true;
                    transferingY = true;
                }
            
        }
        if (!transferingY) {
            
            targetPoint = new Vector3(transform.position.x, player.position.y+y, -1);

            if (!player.GetComponent<PlayerController>().onPlatform && player.GetComponent<PlayerController>().grounded)
                {
                    mid = false;
                    transferingY = true;
                }
            
        }
        if (transferingY) {
            


            transferSpeedY = Vector3.Distance(transform.position, targetPoint) * Time.deltaTime * 2;

            transform.position += Vector3.MoveTowards(transform.position, targetPoint, transferSpeedY) - transform.position;

            if (transform.position.y <= targetPoint.y + fixFloat && transform.position.y >= targetPoint.y - fixFloat)
                {
                    transferingY = false;
                }
            
        }

        //track the player on big falls
        if(player.position.y < transform.position.y - y*1.5f)
        {
            transform.position = new Vector3(transform.position.x, player.position.y + y*1.5f, -1);
        }
    }
}