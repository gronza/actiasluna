﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class Intro : MonoBehaviour {

    public VideoPlayer video;
    public float time;

	// Use this for initialization
	void Start () {
        video = GetComponent<VideoPlayer>();
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

        if(!video.isPlaying)
        {
            video.Play();
        }

        if(time > video.clip.length - 2 || Input.anyKeyDown)
        {
            StartCoroutine(FadeOut(SceneManager.GetActiveScene().buildIndex + 1));
            PlayerPrefs.SetInt("levels", SceneManager.GetActiveScene().buildIndex);
            gameObject.GetComponent<CameraFade>().FadeOut(1);
        }
        
	}

    public IEnumerator FadeOut(int index)
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(index);
    }
}
