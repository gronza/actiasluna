﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapsingPlatform : MonoBehaviour {


    private Rigidbody2D rb;
    public float fallDelay;

    public float spawnDelay;
	public GameObject spawnPosition;
    public GameObject platform;
    GameObject instance;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}

	void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            StartCoroutine(Fall());
            StartCoroutine(Spawn());
        }
    }

    //GET OUTTA HERE
    IEnumerator Fall()
        {
            yield return new WaitForSeconds(fallDelay);
            rb.isKinematic = false;
            GetComponent<Collider2D>().isTrigger = true;
            yield return 0;       
        }

    //...hold on, I still need you
    IEnumerator Spawn()
        {
            yield return new WaitForSeconds(spawnDelay);
            rb.isKinematic = true;
            GetComponent<Collider2D>().isTrigger = false;
            instance =(GameObject)Instantiate(platform, spawnPosition.transform.position, Quaternion.identity);
        }

}
