﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {

    public Transform playerPos;
    public float speed = 10;

	// Use this for initialization
	void Start () {
        playerPos = GameObject.Find("Camera").transform;
	}
	
	// Update is called once per frame
	void Update () {
        //transform.SetPositionAndRotation(new Vector3(playerPos.position.x - playerPos.position.x / speed,
        //    playerPos.position.y - playerPos.position.y / speed - 12), new Quaternion());

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(playerPos.position.x - playerPos.position.x / speed,
            playerPos.position.y - playerPos.position.y / speed - 12), 100f);
	}
}
