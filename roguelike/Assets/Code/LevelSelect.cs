﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

        public Button Intro;
        public Button b1;
        public Button b2;
        public Button b3;
        public Button b4;
        public Button b5;
        public Button b6;
        public Button Boss;

    public CameraFade camera;

	// Use this for initialization
	void Start () {


        if (!PlayerPrefs.HasKey("levels"))
        {
            PlayerPrefs.SetInt("levels", SceneManager.GetActiveScene().buildIndex);
        }

        Intro = transform.GetChild(0).GetComponent<Button>();
        b1 = transform.GetChild(1).GetComponent<Button>();
        b2 = transform.GetChild(2).GetComponent<Button>();
        b3 = transform.GetChild(3).GetComponent<Button>();
        b4 = transform.GetChild(4).GetComponent<Button>();
        b5 = transform.GetChild(5).GetComponent<Button>();
        b6 = transform.GetChild(6).GetComponent<Button>();
        Boss = transform.GetChild(7).GetComponent<Button>();

        Intro.onClick.AddListener(IntroButton);
        b1.onClick.AddListener(Button1);
        b2.onClick.AddListener(Button2);
        b3.onClick.AddListener(Button3);
        b4.onClick.AddListener(Button4);
        b5.onClick.AddListener(Button5);
        b6.onClick.AddListener(Button6);
        Boss.onClick.AddListener(BossButton);

        //get scores for each level from playerprefs
        b1.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("2");
        b2.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("3");
        b3.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("4");
        b4.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("5");
        b5.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("6");
        b6.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("7");

        //get friends found from each level from playerprefs
        b1.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("2bugs");
        b2.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("3bugs");
        b3.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("4bugs");
        b4.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("5bugs");
        b5.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("6bugs");
        b6.gameObject.transform.GetChild(3).transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetString("7bugs");
        Boss.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString("8bugs");

        camera = GameObject.Find("Main Camera").GetComponent<CameraFade>();
    }
	
	// Update is called once per frame
	void Update () {
        var unlocked = PlayerPrefs.GetInt("levels");
        Debug.Log(unlocked);

        if(unlocked >= 1)
        {
            b1.transform.GetChild(2).gameObject.SetActive(false);
            b1.transform.GetChild(3).gameObject.SetActive(true);
            if (unlocked >= 2)
            {
                b2.transform.GetChild(2).gameObject.SetActive(false);
                b2.transform.GetChild(3).gameObject.SetActive(true);
                if (unlocked >= 3)
                {
                    b3.transform.GetChild(2).gameObject.SetActive(false);
                    b3.transform.GetChild(3).gameObject.SetActive(true);
                    if (unlocked >= 4)
                    {
                        b4.transform.GetChild(2).gameObject.SetActive(false);
                        b4.transform.GetChild(3).gameObject.SetActive(true);
                        if (unlocked >= 5)
                        {
                            b5.transform.GetChild(2).gameObject.SetActive(false);
                            b5.transform.GetChild(3).gameObject.SetActive(true);
                            if (unlocked >= 6)
                            {
                                b6.transform.GetChild(2).gameObject.SetActive(false);
                                b6.transform.GetChild(3).gameObject.SetActive(true);
                                if (unlocked >= 7)
                                {
                                    Boss.transform.GetChild(2).gameObject.SetActive(false);
                                    Boss.transform.GetChild(3).gameObject.SetActive(true);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    public void IntroButton()
    {

        StartCoroutine(FadeOut(1));
        camera.FadeOut();
    }

    public void Button1()
    {
        Debug.Log("sprölölööö");
        if (PlayerPrefs.GetInt("levels") >= 1)
        {
            StartCoroutine(FadeOut(2));
            camera.FadeOut();
        }
    }

    public void Button2()
    {
        Debug.Log("sprölölööö");
        if (PlayerPrefs.GetInt("levels") >= 2)
        {
            StartCoroutine(FadeOut(3));
            camera.FadeOut();
        }
    }

    public void Button3()
    {
        if (PlayerPrefs.GetInt("levels") >= 3)
        {
            StartCoroutine(FadeOut(4));
            camera.FadeOut();
        }
    }

    public void Button4()
    {
        if (PlayerPrefs.GetInt("levels") >= 4)
        {
            StartCoroutine(FadeOut(5));
            camera.FadeOut();
        }
    }

    public void Button5()
    {
        if (PlayerPrefs.GetInt("levels") >= 5)
        {
            StartCoroutine(FadeOut(6));
            camera.FadeOut();
        }
    }

    public void Button6()
    {
        if (PlayerPrefs.GetInt("levels") >= 6)
        {
            StartCoroutine(FadeOut(7));
            camera.FadeOut(); ;
        }
    }

    public void BossButton()
    {
        if (PlayerPrefs.GetInt("levels") >= 7)
        {
            StartCoroutine(FadeOut(8));
            camera.FadeOut();
        }
    }

    public IEnumerator FadeOut(int index)
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(index);
    }
}
