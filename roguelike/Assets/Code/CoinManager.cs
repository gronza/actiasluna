﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {

    public GameObject soundEffect;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            GetComponent<AudioSource>().Play();

            var se = Instantiate(soundEffect);
            Destroy(se, se.GetComponent<AudioSource>().clip.length);
            
            Debug.Log("Coin");
            GameObject.Find("Player").GetComponent<StatManager>().score++;
            Destroy(gameObject);
        }
    }
}
